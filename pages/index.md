<!--
.. title: Ubuntu MATE Boutique
.. slug: index
.. date: 2015-06-04 23:01:09 UTC
.. tags: Ubuntu MATE,Shop,Store,Boutique,Stickers,Shirts,Computers,Laptops,Desktops,USB,Entroware
.. link:
.. description:
.. type: text
-->

<div class="row">
  <div class="col-lg-12">
    <div class="well bs-component">
    <a href="https://entroware.com"><img class="centered" src="/images/sponsors/entroware.png" alt="Entroware" /></a>
    </div>
  </div>
</div>

[Entroware](https://entroware.com) is a UK based Linux computer
manufacturer, founded in early 2014, providing a range of quality Linux
computers focused on a complete *"out of the box"* Linux experience,
with a heavy focus on hardware compatibility. Entroware are offering
the option to purchase their entire range of computers with Ubuntu MATE
pre-installed including full support.

## Entroware Laptops

<div class="row">
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/triton">Triton</a>
        <a class="list-group-item" href="https://www.entroware.com/store/triton"><img class="centered" src="/images/merch/entroware/triton_front_mate.jpg" /></a>
	      <p class="list-group-item">Starting at <b>£299</b></p>
        <p class="list-group-item">With Intel Skylake options
        available, Triton is ideal for people who want to secure a
        future proof Linux platform without breaking the bank. With
        Excellent battery life and connectivity, Triton is particularly
        ideal for practical people who need to work on the move.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/orion">Orion</a>
        <a class="list-group-item" href="https://www.entroware.com/store/orion"><img class="centered" src="/images/merch/entroware/orion_front_mate.jpg" /></a>
	      <p class="list-group-item">Starting at <b>£379</b></p>
        <p class="list-group-item">Orion is designed with portability
        and style in mind. With verified Linux compatible components
        and upgradeability. including an i7, grab your Ubuntu laptop
        for home and business today.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/apollo">Apollo</a>
        <a class="list-group-item" href="https://www.entroware.com/store/apollo"><img class="centered" src="/images/merch/entroware/apollo_new_mate.jpg" /></a>
        <p class="list-group-item">Starting at <b>£499</b></p>
        <p class="list-group-item">In an elegant aluminium chassis,
        Apollo is the flagship ultrabook in the Ubuntu laptop range.
        With the latest Intel Skylake CPUs, this is the must have
        Ultrabook for Linux enthusiasts.</p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/kratos">Kratos</a>
        <a class="list-group-item" href="https://www.entroware.com/store/kratos"><img class="centered" src="/images/merch/entroware/kratos_front_mate.jpg" /></a>
	      <p class="list-group-item">Starting at <b>£599</b></p>
        <p class="list-group-item">With highly versatile configuration
        options and an Nvidia GTX 950M, Kratos can be adapted to best
        suit your Linux requirements. Switchable graphics means that
        you can choose to prioritise raw power or excellent battery
        life on the fly.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/proteus">Proteus</a>
        <a class="list-group-item" href="https://www.entroware.com/store/proteus"><img class="centered" src="/images/merch/entroware/proteus_front_mate.jpg" /></a>
        <p class="list-group-item">Starting at <b>£629</b></p>
        <p class="list-group-item">A 1080p screen and backlit keyboard,
        the Proteus is our flagship Ubuntu laptop. With a dedicated
        Nvidia GeForce GTX 960M chip, Entroware recommends Proteus for
        playing through your Linux Steam library.</p>
      </div>
    </div>
  </div>
</div>

## Entroware Desktops

<div class="row">
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/nyx">Nyx</a>
        <a class="list-group-item" href="https://www.entroware.com/store/nyx"><img class="centered" src="/images/merch/entroware/nyx.jpg" /></a>
        <p class="list-group-item">Starting at <b>£349</b></p>
        <p class="list-group-item">The Nyx is a perfect computer for students, developers
        and mid-range gamers who require a Linux platform. Housed in an elegant and
        minimalist case, performance and looks have never come at a better price.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/aura">Aura</a>
        <a class="list-group-item" href="https://www.entroware.com/store/aura"><img class="centered" src="/images/merch/entroware/aura.jpg" /></a>
        <p class="list-group-item">Starting at <b>£379</b></p>
        <p class="list-group-item">The tiny Aura packs in the latest
        Intel Broadwell i3 and i5 processors. With excellent integrated
        graphics performance and VESA mounting brackets, your school or
        office can enjoy an increase in work space. Aura is perfect for
        many Linux applications, including a home theatre PC,
        development system and teaching platform.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.entroware.com/store/poseidon">Poseidon</a>
        <a class="list-group-item" href="https://www.entroware.com/store/poseidon"><img class="centered" src="/images/merch/entroware/poseidon.jpg"></a>
        <p class="list-group-item">Starting at <b>£849</b></p>
        <p class="list-group-item">When it comes to extreme Linux
        desktops, Poseidon dominates its competition. Entroware
        recommends this model for the most meticulous Ubuntu and Ubuntu
        MATE gamers, where every frame and pixel counts.</p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="well bs-component">
    <a href="https://www.libretrend.com/en/"><img class="centered" src="/images/sponsors/libretrend-black.png" alt="LibreTrend" /></a>
    </div>
  </div>
</div>

LibreTrend are the designer and manufacturer of the
[LibreBox](http://www.libretrend.com/en/hardware), a computer geared
towards providing a complete *"out of the box"* Linux experience, with
a heavy focus on hardware compatibility. All the hardware in the
LibreBox is Free Software friendly and 100% supported by *"blobless"*
Linux drivers.

## LibreBox

<div class="row">
  <div class="col-lg-3">
    <div class="bs-component">&nbsp;</div>
  </div>
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="http://www.libretrend.com/en/store/librebox">LibreBox</a>
        <a class="list-group-item" href="http://www.libretrend.com/en/store/librebox"><img class="centered" src="/images/merch/libretrend/LB_TripleBox_0.jpg" /></a>
	      <p class="list-group-item">Starting at <b>&euro;295</b></p>
        <p class="list-group-item">LibreBox delivers the power of Free
        Software in a small, beautiful, black alloy case. The LibreBox
        versatility makes it ideal for any company, organization,
        public establishment and home use. The LibreBox is built with
        verified GNU/Linux compatible components and designed with Free
        Software in mind.</p>
      </div>
    </div>
  </div>
  <div class="col-lg-3">
    <div class="bs-component">&nbsp;</div>
  </div>
</div>

## Embroidered Shirts

[HELLOTUX](https://www.hellotux.com/) is the family business of Gábor Kum,
a Linux system administrator, software developer and Linux user since 1999.
HELLOTUX started making their Linux shirts in 2002 and carefully embroider
every shirt individually using a programmable embroidery machine, all powered
by Linux. **HELLOTUX is an international store that delivers worldwide.**

<div class="row">
  <div class="col-lg-12">
    <div class="bs-component">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/sponsors/hellotux.png" alt="HELLOTUX" /></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntu-mate-polo-kiwi-front.jpg" alt="Ubuntu MATE Polo - Front" /></a>
        <br />
        <b>Ubuntu MATE Polo - Front</b>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntu-mate-polo-kiwi-back.jpg" alt="Ubuntu MATE Polo - Back" /></a>
        <br />
        <b>Ubuntu MATE Polo - Back</b>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntumate_jacket_black-1.png" alt="Ubuntu MATE Jacket" /></a>
        <br />
        <b>Ubuntu MATE Jacket- Black</b>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntumate_jacket_black-2.png" alt="Ubuntu MATE Jacket" /></a>
        <br />
        <b>Ubuntu MATE Jacket- Black</b>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntu-mate-t-white-front.jpg" alt="Ubuntu MATE T-Shirt - Front" /></a>
        <br />
        <b>Ubuntu MATE T-Shirt - Front</b>
    </div>
  </div>
  <div class="col-lg-2">
    <div class="bs-component" align="center">
        <a href="https://www.hellotux.com/ubuntu-mate"><img class="centered" src="/images/merch/hellotux/ubuntu-mate-t-white-back.jpg" alt="Ubuntu MATE T-Shirt - Back" /></a>
        <br />
        <b>Ubuntu MATE T-Shirt - Back</b>
    </div>
  </div>
</div>

## Stickers and Badges

[unixstickers.com](http://www.unixstickers.com/tag/ubuntumate), give your case
the identity it deserves! Our stickers are made from a sheet of printed vinyl,
covered by a layer of top quality transparent film to protect the ink and
improve durability and beauty, then finely cut to follow the shape we want.
**unixstickers.com is an international store that delivers worldwide.**

<div class="row">
  <div class="col-lg-12">
    <div class="well bs-component">
        <a href="http://www.unixstickers.com/tag/ubuntumate"><img class="centered" src="/images/sponsors/unixstickers.png" alt="unixstickers.com" /></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-4">
    <div class="bs-component" align="center">
        <a href="http://www.unixstickers.com/tag/ubuntumate"><img class="centered" src="/images/merch/unixstickers/Ubuntu-MATE-Badge.png" alt="Ubuntu MATE - Badge" /></a>
        <br />
        <b>Ubuntu MATE - Badge</b>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component" align="center">
        <a href="http://www.unixstickers.com/tag/ubuntumate"><img class="centered" src="/images/merch/unixstickers/Ubuntu-MATE-Pin.png" alt="Ubuntu MATE - Pin" /></a>
        <br />
        <b>Ubuntu MATE - Pin</b>
    </div>
  </div>
  <div class="col-lg-4">
    <div class="bs-component" align="center">
        <a href="http://www.unixstickers.com/tag/ubuntumate"><img class="centered" src="/images/merch/unixstickers/Ubuntu-MATE-Keyboard.png" alt="Ubuntu MATE - Keyboard" /></a>
        <br />
        <b>Ubuntu MATE - Keyboard</b>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-lg-12">
    <div class="bs-component" align="center">
        <a href="http://www.unixstickers.com/tag/ubuntumate"><img class="centered" src="/images/merch/unixstickers/Ubuntu-MATE-Shaped.png" alt="Ubuntu MATE - Shaped" /></a>
        <br />
        <b>Ubuntu MATE - Shapes</b>
    </div>
  </div>
</div>

## DVD and USB

<div class="row">
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.osdisc.com/?affiliate=ubuntumate">OSDisc.com</a>
        <a class="list-group-item" href="https://www.osdisc.com/products/ubuntumate?affiliate=ubuntumate">
        <img class="centered" src="/images/sponsors/osdisc.png" alt="OSDisc.com" /></a>
      </div>
        <p><a href="https://www.osdisc.com/products/ubuntumate?affiliate=ubuntumate">OSDisc.com</a> 
        is a leading source for Linux DVDs and USBs. Purchase 
        ready-to-use bootable DVDs and memory sticks that come 
        pre-installed with Ubuntu MATE and have persistent storage.</p> 
        <p><small><i>* Persistent storage is only available on USB 
        sticks that are 32GB or larger</i></small></p>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="bs-component">
      <div class="list-group">
        <a class="list-group-item active" href="https://www.hellotux.com/ubuntumate1510_flash_drive">HELLOTUX</a>
        <a class="list-group-item" href="https://www.hellotux.com/ubuntumate1510_flash_drive">
        <img class="centered" src="/images/merch/hellotux/flashdrive.jpg" alt="Ubuntu MATE Branded Flash Drive" /></a>
      </div>
    </div>
        <p><a href="https://www.hellotux.com/ubuntumate1510_flash_drive">HELLOTUX</a>
        sell an Ubuntu MATE branded 8GB Metallic Unibody USB stick that is just
        41 mm long and less than 5 mm thick. It's the perfect flash drive for
        your key ring, always with you. HELLOTUX will also help you to upgrade
        your flash drive to the next version of Ubuntu MATE, absolutely free.</p>
  </div>  
</div>

## Apparel

Ubuntu MATE has two shops for apparel, one based in Europe and one based in the
Americas. <b>Both shops deliver worldwide and stock the same items.</b>

If you are located in Africa, Asia, Middle East or Ocenia you may want to check
both stores to see which offer the most favourable shipping costs to your
location.

<div class="row">
  <div class="col-lg-6">
    <div class="well bs-component" align="center">
    <a href="https://shop.spreadshirt.co.uk/ubuntu-mate/"><img src="/images/merch/spreadshirt/white-t-shirt.png" alt="102654358-130125103"/></a>
    <a href="https://www.spreadshirt.co.uk/spreadshirt-guarantee-C4070" onclick="window.open(this.href,'','height=520,width=640,scrollbars=yes'); return false;"><img src="/images/sponsors/spreadshirt-guarantee.png" alt="spreadshirt-guarantee" /></a>
    <a href="https://shop.spreadshirt.co.uk/ubuntu-mate/"><img src="/images/merch/spreadshirt/white-mug.png" alt="130128260-102655146"/></a>
    <br />
    <b>Ubuntu MATE European apparel store</b>
    </div>
  </div>
  <div class="col-lg-6">
    <div class="well bs-component" align="center">
    <a href="https://shop.spreadshirt.com/ubuntu-mate/"><img src="/images/merch/spreadshirt/white-t-shirt.png" alt="102309772-1006413448"/></a>
    <a href="https://www.spreadshirt.com/spreadshirt-guarantee-C3570" onclick="window.open(this.href,'','height=500,width=640,scrollbars=yes'); return false;"><img src="/images/sponsors/spreadshirt-guarantee.png" alt="spreadshirt guarantee" /></a>
    <a href="https://shop.spreadshirt.com/ubuntu-mate/"><img src="/images/merch/spreadshirt/white-mug.png" alt="1006415072-102310921" /></a>
    <br />
    <b>Ubuntu MATE Americas apparel store</b>
    </div>
  </div>
</div>
